/* empty css                           */import { c as createAstro, a as createComponent, r as renderTemplate, b as addAttribute, d as renderHead, e as renderSlot, m as maybeRenderHead, f as renderComponent } from '../astro.7eebae14.mjs';
import 'html-escaper';
import 'cookie';
import 'kleur/colors';
import 'slash';
import 'path-to-regexp';
import 'mime';
import 'string-width';

const $$Astro$2 = createAstro();
const $$Layout = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$2, $$props, $$slots);
  Astro2.self = $$Layout;
  const { title } = Astro2.props;
  return renderTemplate`<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="Astro description">
		<meta name="viewport" content="width=device-width">
		<link rel="icon" type="image/svg+xml" href="/favicon.svg">
		<meta name="generator"${addAttribute(Astro2.generator, "content")}>
		<title>${title}</title>
	${renderHead($$result)}</head>
	<body>
		${renderSlot($$result, $$slots["default"])}
	
</body></html>`;
}, "/Users/paaaatrick/Projects/half-foal/src/layouts/Layout.astro");

const $$Astro$1 = createAstro();
const $$Hero = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$1, $$props, $$slots);
  Astro2.self = $$Hero;
  const { cta, href, title, body } = Astro2.props;
  return renderTemplate`${maybeRenderHead($$result)}<section class="h-screen bg-slate-400 border-b-[10px] border-black">
  <h2><a${addAttribute(href, "href")}>${title}</a></h2>
  <p>${body}</p>

  <p>
    <a${addAttribute(href, "href")}>${cta}</a>
  </p>
</section>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/Hero.astro");

const $$Astro = createAstro();
const $$Index = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$Index;
  return renderTemplate`${renderComponent($$result, "Layout", $$Layout, { "title": "Welcome to Astro." }, { "default": ($$result2) => renderTemplate`
	${maybeRenderHead($$result2)}<main>
		${renderComponent($$result2, "Hero", $$Hero, { "title": "Welcome to Astro.", "body": "The fastest way to build your website.", "cta": "Get Started", "href": "https://docs.astro.build/getting-started/" })}
		${renderComponent($$result2, "Hero", $$Hero, { "title": "Welcome to Astro.", "body": "The fastest way to build your website.", "cta": "Get Started", "href": "https://docs.astro.build/getting-started/" })}
		${renderComponent($$result2, "Hero", $$Hero, { "title": "Welcome to Astro.", "body": "The fastest way to build your website.", "cta": "Get Started", "href": "https://docs.astro.build/getting-started/" })}
	</main>
` })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/pages/index.astro");

const $$file = "/Users/paaaatrick/Projects/half-foal/src/pages/index.astro";
const $$url = "";

export { $$Index as default, $$file as file, $$url as url };
