/* empty css                           */import { c as createAstro, a as createComponent, r as renderTemplate, m as maybeRenderHead, s as spreadAttributes, b as addAttribute, u as unescapeHTML, d as renderComponent, F as Fragment, e as renderHead, f as renderSlot } from '../astro.a83e0918.mjs';
import { optimize } from 'svgo';
import 'cookie';
import 'kleur/colors';
import 'slash';
import 'path-to-regexp';
import 'mime';
import 'html-escaper';
import 'string-width';

const __vite_glob_1_0 = "<svg class=\"horse-butt\" viewBox=\"0 0 70.08 202.561\"><g fill-rule=\"evenodd\"><path fill=\"currentColor\" d=\"M10.56.24h6.72c2.91 1.285 3.59 3.415 1.984 6.246-.295.521-.678.996-.94 1.531-.942 1.925-2.261 3.777-2.7 5.816-2.141 9.932-1.375 19.83 1.387 29.517 2.088 7.324 4.791 14.471 7.008 21.761 1.687 5.548 4.824 9.979 9.125 13.777 4.491 3.965 8.954 7.982 13.174 12.23 6.409 6.453 10.898 14.047 12.611 23.11.126.666.221 1.346.426 1.987.266.828.84 1.195 1.661.691 1.109-.681 2.185-1.419 3.434-2.236-3.24-1.36-4.64-3.991-5.635-6.938-.217-.643-.444-1.281-.672-1.92-3.034-8.507-8.638-15.114-15.419-20.85-3.446-2.915-7.106-5.616-10.289-8.794-6.18-6.17-8.794-13.488-6.503-22.295 2.58-9.92 2.844-19.973 1.036-30.077-.417-2.325.228-4.392 1.695-6.275a249.112 249.112 0 0 0 6.304-8.444c1.99-2.788 7.79-5.379 11.064-4.462 3.327.931 5.294 3.347 3.297 6.864-.354.624-.828 1.181-1.172 1.809-1.355 2.478-2.934 4.871-3.967 7.479-2.282 5.764-2.543 11.913-2.865 18.021-.548 10.413-1.062 20.827-1.618 31.238-.201 3.767.873 7.029 3.673 9.614a283.911 283.911 0 0 0 6.972 6.23c2.75 2.378 5.705 4.541 8.298 7.077 4.021 3.932 6.186 8.96 7.618 14.323.47 1.755 1.137 3.29 3.534 3.393 0 4.915-.031 9.749.007 14.581.068 8.715.169 17.429.281 26.143.022 1.778.151 3.555.231 5.331v6.48c-3.151.16-6.316.581-9.451.421-5.257-.268-10.525-.699-15.732-1.454-4.144-.601-8.854 2.828-6.761 8.589 1.118 3.077.933 6.043-.545 8.926-1.001 1.955-.898 3.821.158 5.708.624 1.113 1.351 2.213 1.714 3.415.419 1.386.668 2.879.651 4.32-.013 1.077.082 1.99.831 2.739a50.258 50.258 0 0 0 2.796 2.585c.866.743 1.525 1.582 1.229 2.773-.262 1.055-1.239 1.605-2.636 1.57-2.156-.053-4.319-.057-6.464-.252-4.888-.445-9.456-1.85-13.537-4.691-3.495-2.433-5.024-5.825-4.908-9.966.1-3.563-.769-6.764-2.809-9.699a12.269 12.269 0 0 1-1.662-3.421c-1.68-5.613 1.146-12.729 6.571-15.791 3.196-1.804 5.959-4.119 8.873-6.269.599-.441 1.166-.925 1.806-1.435-.396-.681-.726-1.269-1.075-1.845-.909-1.501-1.929-2.945-2.725-4.504-4.494-8.799-4.6-18.151-3.162-27.611.416-2.735 1.597-5.347 2.25-8.058.227-.94.29-2.261-.2-2.978-3.382-4.946-6.792-9.896-11.594-13.646-3.032-2.367-5.116-5.393-6.005-9.135-.468-1.97-.825-4.003-.913-6.021-.32-7.299-.437-14.611-.763-21.913-.25-5.625-.511-11.262-1.105-16.857C4.512 32.669 2.021 27.101.24 21.36V18c1.024-2.146 2.003-4.315 3.086-6.431 1.402-2.738 2.741-5.474 3.072-8.604.058-.545.521-1.245.998-1.506C8.379.921 9.498.632 10.56.24zm19.768 178.082c2.2-1.616 2.595-3.727 1.249-6.373-2.26 1.142-2.701 4.311-1.249 6.373zm2.479-19.552c-3.59-.393-5.775 1.325-5.098 3.904 2.466-.218 3.96-1.689 5.098-3.904zm-8.601 11.52c-2.059 1.492-2.939 2.788-2.877 4.309.011.26.187.651.383.724.202.075.611-.09.771-.282.637-.767 1.181-1.611 1.819-2.378.667-.802.732-1.542-.096-2.373zm4.588 22.487c2.184-3.28 1.606-5.215-1.548-5.457l1.548 5.457zm5.771 5.072l2.712 2.9.808-.974-2.45-2.854-1.07.928z\"></path><path fill=\"#FFF\" d=\"M30.328 178.322c-1.451-2.063-1.011-5.231 1.249-6.373 1.346 2.647.951 4.757-1.249 6.373zM32.807 158.77c-1.138 2.216-2.632 3.687-5.098 3.904-.677-2.579 1.508-4.297 5.098-3.904zM24.206 170.29c.828.831.763 1.571.096 2.372-.638.767-1.182 1.611-1.819 2.378-.16.192-.569.357-.771.282-.196-.072-.372-.464-.383-.724-.062-1.52.818-2.816 2.877-4.308zM28.794 192.777l-1.548-5.457c3.155.243 3.733 2.177 1.548 5.457zM34.565 197.849l1.07-.928 2.45 2.854-.808.974-2.712-2.9z\"></path></g></svg>";

const __vite_glob_1_1 = "<svg class=\"horse-head\" viewBox=\"0 0 96 208.08\"><g fill-rule=\"evenodd\" fill=\"currentColor\"><path d=\"M96.24 61.68c-.83 2.769-2.081 5.281-4.514 7.002-1.6 1.132-3.389 2.107-5.347 1.334-1.194-.472-2.38-1.393-3.158-2.423-4.259-5.638-9.683-10.071-14.931-14.683-2.038-1.791-4.317-1.419-5.309 1.001a197.604 197.604 0 0 0-5.122 13.923c-.859 2.646-.286 5.414.529 8.031 1.142 3.664.601 6.95-1.584 10.119-1.399 2.03-2.492 4.274-3.689 6.441-1.188 2.151-.619 4.456-.478 6.706.516 8.214 1.018 16.43 1.627 24.638.313 4.222 1.049 8.42 1.218 12.644.208 5.181.013 10.377.014 15.567.001 3.276.042 6.553.065 9.83.024 3.354-.08 6.714.103 10.058.354 6.503 2.881 12.18 6.693 17.416 1.938 2.661 3.525 5.595 5.105 8.495 1.019 1.869.094 3.895-1.95 4.481-5.14 1.474-9.36.446-12.159-4.425-2.267-3.946-4.406-7.969-6.477-12.022-1.607-3.146-1.813-6.571-1.642-10.053.48-9.818.913-19.64 1.363-29.46.393-8.583.872-17.163 1.146-25.749.175-5.47-.067-10.952.088-16.422.129-4.551.571-9.093.857-13.64.047-.731.007-1.468.007-2.499-2.684 1.862-4.343 3.607-4.167 6.928.521 9.855.933 19.727.968 29.593.034 9.348-.458 18.696-.729 28.045-.208 7.15-.403 14.301-.66 21.449-.107 2.994-.418 5.98-.533 8.975-.188 4.902 1.081 9.577 2.396 14.235.684 2.42 1.43 4.823 2.096 7.248.647 2.357-.503 3.822-2.962 3.855-3.223.043-4.763-1.078-5.992-4.079-1.3-3.177-2.592-6.37-4.14-9.429-2.733-5.4-3.428-11.024-2.791-16.994a221.47 221.47 0 0 0 1.179-17.092c.412-14.258-.917-28.424-2.575-42.566-.6-5.116-1.042-10.253-1.677-15.365a7.83 7.83 0 0 0-1.246-3.325c-2.38-3.491-4.905-6.884-7.375-10.314l-.577.205c.044.375-.013.802.148 1.118 1.669 3.273 3.373 6.528 5.066 9.789.146.282.283.569.546 1.101-2.088.376-4.058.83-6.054 1.071-5.565.673-11.094.104-16.601-.708-1.712-.252-2.257-.93-2.289-2.802a2732.63 2732.63 0 0 1-.244-18.109C.447 77.3.459 73.78.431 70.26c-.003-.38-.125-.76-.191-1.14V49.68c3.263-.281 6.521-.69 9.79-.816 6.6-.255 12.017-2.692 15.864-8.189 1.236-1.766 2.293-3.657 3.54-5.414 4.026-5.671 9.432-9.917 14.647-14.411 8.419-7.254 18.379-9.187 28.997-9.287 5.309-.05 10.618-.044 15.927-.035.395 0 .877.134 1.143.389.124.119-.105.717-.292 1.028-.91 1.52-1.859 3.016-2.879 4.657 1.244.197 2.263.346 3.276.521 1.337.232 1.722 1.276.841 2.37-.346.43-.787.796-1.229 1.134-1.139.869-2.302 1.706-3.451 2.553.042.174.052.294.099.397.215.473.41.957.666 1.407 2.038 3.586 3.357 7.423 3.852 11.51.346 2.852.481 5.729.743 8.591.403 4.398 2.405 8.208 4.468 11.993-.002 1.202-.002 2.402-.002 3.602zm-15.594-9.36c3.306-.003 5.434-1.42 5.92-4.104.518-2.857.013-5.573-1.59-8.019-1.103-1.684-2.691-2.709-4.692-3.015-5.529-.844-9.512 4.054-7.896 9.691.954 3.327 4.174 5.451 8.258 5.447z\"></path><path d=\"M74.88.24l5.473 7.438c-.642.284-1.166.699-1.712.729-3.188.177-6.383.231-9.571.388-6.984.345-13.328 2.705-19.327 6.168-9.231 5.331-16.637 12.63-22.941 21.137-2.188 2.953-4.442 5.855-6.661 8.776-2.273-1.445-2.853-3.849-2.704-11.194l5.523 2.258-1.221-6.175c1.459.274 2.73.552 4.015.744 1.432.214 1.895-.218 1.665-1.63-.204-1.25-.557-2.477-.875-3.845l6.819-.402-.454-6.734h7.327l.857-7.686 7.376 1.331.442-5.84 7.382 2.805 2.232-5.913c1.379.975 2.708 1.836 3.949 2.809 1.839 1.44 2.645 1.267 3.595-.887.226-.511.479-1.011.749-1.499.288-.522.612-1.024.989-1.649l3.836 4.078L74.639.239l.241.001zM81.833 48.046H77.88c0-.882.025-1.715-.008-2.546-.036-.905.315-1.437 1.251-1.565 1.461-.201 2.277.126 2.577 1.095.069.227.121.468.127.704.017.713.006 1.428.006 2.312z\"></path></g></svg>";

const SPRITESHEET_NAMESPACE = `astroicon`;

const baseURL = "https://api.astroicon.dev/v1/";
const requests = /* @__PURE__ */ new Map();
const fetchCache = /* @__PURE__ */ new Map();
async function get(pack, name) {
  const url = new URL(`./${pack}/${name}`, baseURL).toString();
  if (requests.has(url)) {
    return await requests.get(url);
  }
  if (fetchCache.has(url)) {
    return fetchCache.get(url);
  }
  let request = async () => {
    const res = await fetch(url);
    if (!res.ok) {
      throw new Error(await res.text());
    }
    const contentType = res.headers.get("Content-Type");
    if (!contentType.includes("svg")) {
      throw new Error(`[astro-icon] Unable to load "${name}" because it did not resolve to an SVG!

Recieved the following "Content-Type":
${contentType}`);
    }
    const svg = await res.text();
    fetchCache.set(url, svg);
    requests.delete(url);
    return svg;
  };
  let promise = request();
  requests.set(url, promise);
  return await promise;
}

const splitAttrsTokenizer = /([a-z0-9_\:\-]*)\s*?=\s*?(['"]?)(.*?)\2\s+/gim;
const domParserTokenizer = /(?:<(\/?)([a-zA-Z][a-zA-Z0-9\:]*)(?:\s([^>]*?))?((?:\s*\/)?)>|(<\!\-\-)([\s\S]*?)(\-\->)|(<\!\[CDATA\[)([\s\S]*?)(\]\]>))/gm;
const splitAttrs = (str) => {
  let res = {};
  let token;
  if (str) {
    splitAttrsTokenizer.lastIndex = 0;
    str = " " + (str || "") + " ";
    while (token = splitAttrsTokenizer.exec(str)) {
      res[token[1]] = token[3];
    }
  }
  return res;
};
function optimizeSvg(contents, name, options) {
  return optimize(contents, {
    plugins: [
      "removeDoctype",
      "removeXMLProcInst",
      "removeComments",
      "removeMetadata",
      "removeXMLNS",
      "removeEditorsNSData",
      "cleanupAttrs",
      "minifyStyles",
      "convertStyleToAttrs",
      {
        name: "cleanupIDs",
        params: { prefix: `${SPRITESHEET_NAMESPACE}:${name}` }
      },
      "removeRasterImages",
      "removeUselessDefs",
      "cleanupNumericValues",
      "cleanupListOfValues",
      "convertColors",
      "removeUnknownsAndDefaults",
      "removeNonInheritableGroupAttrs",
      "removeUselessStrokeAndFill",
      "removeViewBox",
      "cleanupEnableBackground",
      "removeHiddenElems",
      "removeEmptyText",
      "convertShapeToPath",
      "moveElemsAttrsToGroup",
      "moveGroupAttrsToElems",
      "collapseGroups",
      "convertPathData",
      "convertTransform",
      "removeEmptyAttrs",
      "removeEmptyContainers",
      "mergePaths",
      "removeUnusedNS",
      "sortAttrs",
      "removeTitle",
      "removeDesc",
      "removeDimensions",
      "removeStyleElement",
      "removeScriptElement"
    ]
  }).data;
}
const preprocessCache = /* @__PURE__ */ new Map();
function preprocess(contents, name, { optimize }) {
  if (preprocessCache.has(contents)) {
    return preprocessCache.get(contents);
  }
  if (optimize) {
    contents = optimizeSvg(contents, name);
  }
  domParserTokenizer.lastIndex = 0;
  let result = contents;
  let token;
  if (contents) {
    while (token = domParserTokenizer.exec(contents)) {
      const tag = token[2];
      if (tag === "svg") {
        const attrs = splitAttrs(token[3]);
        result = contents.slice(domParserTokenizer.lastIndex).replace(/<\/svg>/gim, "").trim();
        const value = { innerHTML: result, defaultProps: attrs };
        preprocessCache.set(contents, value);
        return value;
      }
    }
  }
}
function normalizeProps(inputProps) {
  const size = inputProps.size;
  delete inputProps.size;
  const w = inputProps.width ?? size;
  const h = inputProps.height ?? size;
  const width = w ? toAttributeSize(w) : void 0;
  const height = h ? toAttributeSize(h) : void 0;
  return { ...inputProps, width, height };
}
const toAttributeSize = (size) => String(size).replace(/(?<=[0-9])x$/, "em");
async function load(name, inputProps, optimize) {
  const key = name;
  if (!name) {
    throw new Error("<Icon> requires a name!");
  }
  let svg = "";
  let filepath = "";
  if (name.includes(":")) {
    const [pack, ..._name] = name.split(":");
    name = _name.join(":");
    filepath = `/src/icons/${pack}`;
    let get$1;
    try {
      const files = /* #__PURE__ */ Object.assign({

});
      const keys = Object.fromEntries(
        Object.keys(files).map((key2) => [key2.replace(/\.[cm]?[jt]s$/, ""), key2])
      );
      if (!(filepath in keys)) {
        throw new Error(`Could not find the file "${filepath}"`);
      }
      const mod = files[keys[filepath]];
      if (typeof mod.default !== "function") {
        throw new Error(
          `[astro-icon] "${filepath}" did not export a default function!`
        );
      }
      get$1 = mod.default;
    } catch (e) {
    }
    if (typeof get$1 === "undefined") {
      get$1 = get.bind(null, pack);
    }
    const contents = await get$1(name, inputProps);
    if (!contents) {
      throw new Error(
        `<Icon pack="${pack}" name="${name}" /> did not return an icon!`
      );
    }
    if (!/<svg/gim.test(contents)) {
      throw new Error(
        `Unable to process "<Icon pack="${pack}" name="${name}" />" because an SVG string was not returned!

Recieved the following content:
${contents}`
      );
    }
    svg = contents;
  } else {
    filepath = `/src/icons/${name}.svg`;
    try {
      const files = /* #__PURE__ */ Object.assign({"/src/icons/horse-butt.svg": __vite_glob_1_0,"/src/icons/horse-head.svg": __vite_glob_1_1});
      if (!(filepath in files)) {
        throw new Error(`Could not find the file "${filepath}"`);
      }
      const contents = files[filepath];
      if (!/<svg/gim.test(contents)) {
        throw new Error(
          `Unable to process "${filepath}" because it is not an SVG!

Recieved the following content:
${contents}`
        );
      }
      svg = contents;
    } catch (e) {
      throw new Error(
        `[astro-icon] Unable to load "${filepath}". Does the file exist?`
      );
    }
  }
  const { innerHTML, defaultProps } = preprocess(svg, key, { optimize });
  if (!innerHTML.trim()) {
    throw new Error(`Unable to parse "${filepath}"!`);
  }
  return {
    innerHTML,
    props: { ...defaultProps, ...normalizeProps(inputProps) }
  };
}

const $$Astro$f = createAstro();
const $$Icon = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$f, $$props, $$slots);
  Astro2.self = $$Icon;
  let { name, pack, title, optimize = true, class: className, ...inputProps } = Astro2.props;
  let props = {};
  if (pack) {
    name = `${pack}:${name}`;
  }
  let innerHTML = "";
  try {
    const svg = await load(name, { ...inputProps, class: className }, optimize);
    innerHTML = svg.innerHTML;
    props = svg.props;
  } catch (e) {
    {
      throw new Error(`[astro-icon] Unable to load icon "${name}"!
${e}`);
    }
  }
  return renderTemplate`${maybeRenderHead($$result)}<svg${spreadAttributes(props)}${addAttribute(name, "astro-icon")}>${unescapeHTML((title ? `<title>${title}</title>` : "") + innerHTML)}</svg>`;
}, "/Users/paaaatrick/Projects/half-foal/node_modules/astro-icon/lib/Icon.astro");

const sprites = /* @__PURE__ */ new WeakMap();
function trackSprite(request, name) {
  let currentSet = sprites.get(request);
  if (!currentSet) {
    currentSet = /* @__PURE__ */ new Set([name]);
  } else {
    currentSet.add(name);
  }
  sprites.set(request, currentSet);
}
const warned = /* @__PURE__ */ new Set();
async function getUsedSprites(request) {
  const currentSet = sprites.get(request);
  if (currentSet) {
    return Array.from(currentSet);
  }
  if (!warned.has(request)) {
    const { pathname } = new URL(request.url);
    console.log(`[astro-icon] No sprites found while rendering "${pathname}"`);
    warned.add(request);
  }
  return [];
}

const $$Astro$e = createAstro();
const $$Spritesheet = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$e, $$props, $$slots);
  Astro2.self = $$Spritesheet;
  const { optimize = true, style, ...props } = Astro2.props;
  const names = await getUsedSprites(Astro2.request);
  const icons = await Promise.all(names.map((name) => {
    return load(name, {}, optimize).then((res) => ({ ...res, name })).catch((e) => {
      {
        throw new Error(`[astro-icon] Unable to load icon "${name}"!
${e}`);
      }
    });
  }));
  return renderTemplate`${maybeRenderHead($$result)}<svg${addAttribute(`position: absolute; width: 0; height: 0; overflow: hidden; ${style ?? ""}`.trim(), "style")}${spreadAttributes({ "aria-hidden": true, ...props })} astro-icon-spritesheet>
    ${icons.map((icon) => renderTemplate`<symbol${spreadAttributes(icon.props)}${addAttribute(`${SPRITESHEET_NAMESPACE}:${icon.name}`, "id")}>${unescapeHTML(icon.innerHTML)}</symbol>`)}
</svg>`;
}, "/Users/paaaatrick/Projects/half-foal/node_modules/astro-icon/lib/Spritesheet.astro");

const $$Astro$d = createAstro();
const $$SpriteProvider = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$d, $$props, $$slots);
  Astro2.self = $$SpriteProvider;
  const content = await Astro2.slots.render("default");
  return renderTemplate`${renderComponent($$result, "Fragment", Fragment, {}, { "default": ($$result2) => renderTemplate`${unescapeHTML(content)}` })}
${renderComponent($$result, "Spritesheet", $$Spritesheet, {})}`;
}, "/Users/paaaatrick/Projects/half-foal/node_modules/astro-icon/lib/SpriteProvider.astro");

const $$Astro$c = createAstro();
const $$Sprite = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$c, $$props, $$slots);
  Astro2.self = $$Sprite;
  let { name, pack, title, class: className, x, y, ...inputProps } = Astro2.props;
  const props = normalizeProps(inputProps);
  if (pack) {
    name = `${pack}:${name}`;
  }
  const href = `#${SPRITESHEET_NAMESPACE}:${name}`;
  trackSprite(Astro2.request, name);
  return renderTemplate`${maybeRenderHead($$result)}<svg${spreadAttributes(props)}${addAttribute(className, "class")}${addAttribute(name, "astro-icon")}>
    ${title ? renderTemplate`<title>${title}</title>` : ""}
    <use${spreadAttributes({ "xlink:href": href, width: props.width, height: props.height, x, y })}></use>
</svg>`;
}, "/Users/paaaatrick/Projects/half-foal/node_modules/astro-icon/lib/Sprite.astro");

Object.assign($$Sprite, { Provider: $$SpriteProvider });

const $$Astro$b = createAstro();
const $$Butt = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$b, $$props, $$slots);
  Astro2.self = $$Butt;
  return renderTemplate`${renderComponent($$result, "Icon", $$Icon, { "name": "horse-butt", "class": "astro-OKXGMS32" })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/logo/Butt.astro");

const $$Astro$a = createAstro();
const $$Head$1 = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$a, $$props, $$slots);
  Astro2.self = $$Head$1;
  return renderTemplate`${renderComponent($$result, "Icon", $$Icon, { "name": "horse-head", "class": "astro-P47PDSFY" })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/logo/Head.astro");

const $$Astro$9 = createAstro();
const $$Logo = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$9, $$props, $$slots);
  Astro2.self = $$Logo;
  return renderTemplate`${maybeRenderHead($$result)}<div class="logo astro-P524E4L7" tabindex="1">
  ${renderComponent($$result, "Butt", $$Butt, { "class": "astro-P524E4L7" })}
  ${renderComponent($$result, "Head", $$Head$1, { "class": "astro-P524E4L7" })}
</div>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/logo/Logo.astro");

const $$Astro$8 = createAstro();
const $$MainNav = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$8, $$props, $$slots);
  Astro2.self = $$MainNav;
  return renderTemplate`${maybeRenderHead($$result)}<nav>
  <a href="/">
    ${renderComponent($$result, "Logo", $$Logo, {})}
  </a>
</nav>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/navigation/MainNav.astro");

const $$Astro$7 = createAstro();
const $$Header = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$7, $$props, $$slots);
  Astro2.self = $$Header;
  return renderTemplate`${maybeRenderHead($$result)}<header class="fixed top-6 left-6 z-[1000]">
  ${renderComponent($$result, "MainNav", $$MainNav, {})}
</header>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/header/Header.astro");

const userAgents = [
  // this must always be the first element here!
  {
    name: "woff",
    ua: "Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko"
  },
  // from: https://github.com/fontsource/google-font-metadata/blob/main/data/user-agents.json
  {
    name: "woff2",
    ua: 'Mozilla/5.0 (Windows NT 6.3; rv:39.0) Gecko/20100101 Firefox/44.0"'
  }
];
function downloadFontCSS(url) {
  const fontDownloads = Promise.all(
    userAgents.map((entry) => {
      return fetch(url, { headers: { "User-Agent": entry.ua } }).then((res) => res.text()).then(
        (t) => t.replace(/  +/g, "").replace(/\t+/g, "").replace(/\n+/g, "")
      );
    })
  );
  return fontDownloads.then((contents) => contents.join(" "));
}

const $$Astro$6 = createAstro();
const $$GoogleFontsOptimizer = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$6, $$props, $$slots);
  Astro2.self = $$GoogleFontsOptimizer;
  const props = Astro2.props;
  const urls = Array.isArray(props.url) ? props.url : [props.url];
  const contents = await Promise.all(urls.map((url) => downloadFontCSS(url)));
  return renderTemplate`${contents.length > 0 && renderTemplate`<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">`}
${contents.map(
    (styles) => renderTemplate`${renderComponent($$result, "Fragment", Fragment, {}, { "default": ($$result2) => renderTemplate`
    <style type="text/css">${unescapeHTML(styles)}</style>
` })}`
  )}`;
}, "/Users/paaaatrick/Projects/half-foal/node_modules/astro-google-fonts-optimizer/GoogleFontsOptimizer.astro");

const $$Astro$5 = createAstro();
const $$Head = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$5, $$props, $$slots);
  Astro2.self = $$Head;
  const { title } = Astro2.props;
  return renderTemplate`<head>
  <meta charset="UTF-8">
  <meta name="description" content="Astro description">
  <meta name="viewport" content="width=device-width">
  <link rel="icon" type="image/svg+xml" href="/assets/favicons/favicon.svg">
  <title>${title}</title>
  ${renderComponent($$result, "GoogleFontsOptimizer", $$GoogleFontsOptimizer, { "url": "https://fonts.googleapis.com/css2?family=Eczar:wght@800&family=Inter:wght@900&family=Lato&display=swap" })}
${renderHead($$result)}</head>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/global/head/Head.astro");

const $$Astro$4 = createAstro();
const $$HTML = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$4, $$props, $$slots);
  Astro2.self = $$HTML;
  const { title } = Astro2.props;
  return renderTemplate`<html lang="en">
  ${renderComponent($$result, "Head", $$Head, { "title": title })}
  ${renderSlot($$result, $$slots["default"])}
	
</html>`;
}, "/Users/paaaatrick/Projects/half-foal/src/layouts/HTML.astro");

const $$Astro$3 = createAstro();
const $$Page = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$3, $$props, $$slots);
  Astro2.self = $$Page;
  const { title, description, permalink } = Astro2.props;
  return renderTemplate`${renderComponent($$result, "HTML", $$HTML, { "title": title }, { "default": ($$result2) => renderTemplate`
  
    ${renderComponent($$result2, "Header", $$Header, {})}
    ${maybeRenderHead($$result2)}<main>
      ${renderSlot($$result2, $$slots["default"])}
    </main>
    ` })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/layouts/Page.astro");

const $$Astro$2 = createAstro();
const $$Section = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$2, $$props, $$slots);
  Astro2.self = $$Section;
  const { classes } = Astro2.props;
  return renderTemplate`${maybeRenderHead($$result)}<section${addAttribute(classes, "class")}>
  ${renderSlot($$result, $$slots["default"])}
</section>`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/elements/Section.astro");

const $$Astro$1 = createAstro();
const $$Hero = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$1, $$props, $$slots);
  Astro2.self = $$Hero;
  const { cta, href, title, body, direction } = Astro2.props;
  let bgClass = "noise-forwards contrast-[3]";
  if (direction == "reverse") {
    bgClass = "noise-reverse contrast-[2]";
  }
  return renderTemplate`${renderComponent($$result, "Section", $$Section, { "classes": "h-[200vh] w-full relative", "class": "astro-ANHLOY43" }, { "default": ($$result2) => renderTemplate`
  ${maybeRenderHead($$result2)}<div class="z-10 relative flex justify-center items-center h-screen astro-ANHLOY43">
    <h1 class="font-logo leading-none font-extrabold text-[15vw] text-lemon text-center astro-ANHLOY43">${title}</h1>
  </div>
  <div class="absolute inset-0 w-full h-full astro-ANHLOY43">
    <div class="isolate w-full h-full relative astro-ANHLOY43">
      <div${addAttribute(`${bgClass} h-full astro-ANHLOY43`, "class")}></div>
      <div class="absolute inset-0 w-full h-full bg-hotpink mix-blend-color-dodge astro-ANHLOY43"></div>
    </div>
  </div>
` })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/components/sections/Hero.astro");

const $$Astro = createAstro();
const $$Index = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$Index;
  return renderTemplate`${renderComponent($$result, "Page", $$Page, { "title": "Welcome to Astro." }, { "default": ($$result2) => renderTemplate`
	${maybeRenderHead($$result2)}<main>
		${renderComponent($$result2, "Hero", $$Hero, { "title": "Hi, i'm pat", "body": "The fastest way to build your website.", "cta": "Get Started", "href": "https://docs.astro.build/getting-started/" })}
		${renderComponent($$result2, "Hero", $$Hero, { "body": "The fastest way to build your website.", "cta": "Get Started", "href": "https://docs.astro.build/getting-started/", "direction": "reverse" })}
	</main>
` })}`;
}, "/Users/paaaatrick/Projects/half-foal/src/pages/index.astro");

const $$file = "/Users/paaaatrick/Projects/half-foal/src/pages/index.astro";
const $$url = "";

export { $$Index as default, $$file as file, $$url as url };
