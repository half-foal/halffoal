/** @type {import('tailwindcss').Config} */
module.exports = {
	mode: 'jit',
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		fontFamily: {
      logo: ['Eczar', 'serif'],
      heading: ['Inter', 'sans-serif'],
      body: ['Lato', 'sans-serif'],
    },
		extend: {
			colors: {
				transparent: 'transparent',
				black: '#000',
				white: '#fff',
				lemon: '#EEFE08',
				hotpink: '#F42EDD',
				metal: '#2C3439',
				pink: '#BE30B1',
				violet: '#40334A',
			},
		},
	},
	plugins: [],
}
